public class twoNumbers {
    private static boolean next = false;

    public static String sum(String first, String second) {
        StringBuilder builder = new StringBuilder();
        String little = first.length() > second.length() ? first : second;
        String big = first.length() > second.length() ? second : first;
        String zero = "";
        for (int i = 0; i < big.length() - little.length(); i++) {
            zero = zero + "0";
        }
        little = zero + little;
        char[] lArray = little.toCharArray();
        char[] bArray = big.toCharArray();
        next = false;
        for (int j = big.length() - 1; j >= 0; j--) {
            builder.append(fin(Integer.parseInt(String.valueOf(bArray[j])), Integer.parseInt(String.valueOf(lArray[j]))));
        }
        return builder.reverse().toString();
    }
    private static String fin(int first, int second) {
        String result;
        int sum = first + second;
        if (next) {
            sum++;
        }
        if (sum > 9) {
            next = true;
            result = (sum % 10) + "";
        } else {
            result = sum + "";
            next = false;
        }
        return result;
    }
    public static void main(String[] args) {
        System.out.println(sum("173", "106"));
    }
}
